---
title: Contribute
keywords:
  - contribute
  - inko
  - gitlab
description: How to contribute to the Inko programming language.
---
<!-- vale off -->

Would you like to contribute to the development of Inko? Great! Inko is free and
open source software, so you are free to do so! If you are not sure how to get
involved, then this page will help.

There are many ways to contribute, such as (but not limited to):

* Submitting bug fixes.
* Using Inko, and reporting any bugs you run into.
* Telling others about Inko.
* Hosting meetups and giving presentations.
* Fixing typos on the website, documentation, and other resources.
* Answering questions in the Matrix chat channel(s), on StackOverflow, or other
  mediums.
* Packaging Inko for your favourite operating system(s).
* Adding support for Inko to your favourite text editor or IDE.

Certain projects may have specific contributing guidelines. If this is the case,
they will be found in a file called "CONTRIBUTING.md" at the root of the
repository.

## Source Code

All source code of the Inko project is hosted on
[GitLab.com](https://gitlab.com/inko-lang), spread across several different
repositories. Some of the more commonly used ones are:

* <https://gitlab.com/inko-lang/inko>: the repository containing the compiler,
  runtime, and virtual machine. Also used for reporting bugs, requesting
  features, and more.
* <https://gitlab.com/inko-lang/website>: the source code of this website,
  including the manual. Issues related to the website (e.g. typos) should be
  reported here.
* <https://gitlab.com/inko-lang/inko.vim>: Vim support for Inko.
